[Spring Boot Data MongoDB - 연결 설정하기](https://devel-repository.tistory.com/72)

[Spring Boot Data MongoDB - MongoTemplate 설정하기](https://devel-repository.tistory.com/73)

[Spring Boot Data MongoDB - ScopedValue를 이용한 MongoDB multiple 데이터베이스 연결하기](https://devel-repository.tistory.com/74)

[Spring Boot Data MongoDB - 필드 변환하기 (Converter)](https://devel-repository.tistory.com/75)

[Spring Boot Data MongoDB - Repository 사용에 대한 기초 가이드](https://devel-repository.tistory.com/77)

[Spring Boot Data MongoDB - auditing 설정 및 활용](https://devel-repository.tistory.com/78)

[Spring Boot Data MongoDB - Custom Repository 사용하기](https://devel-repository.tistory.com/80)
* 애플리케이션 구동을 위해서는 JDK 21 버전 환경이 필요합니다.