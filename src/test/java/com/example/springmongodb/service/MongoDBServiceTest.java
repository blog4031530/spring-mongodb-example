package com.example.springmongodb.service;

import com.example.configuration.MongoDBConfigurationByDirect;
import com.example.springmongodb.data.Gender;
import com.example.springmongodb.data.ScopedValues;
import com.example.springmongodb.documents.MailSenderDocument;
import com.example.springmongodb.documents.PersonDocument;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.MongoConnectionDetails;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@ExtendWith( SpringExtension.class )
@Import( MongoDBConfigurationByDirect.class )
@ContextConfiguration( classes = { MongoDBService.class, MongoDBIndexService.class } )
@DataMongoTest
class MongoDBServiceTest {

    @Autowired
    MongoDBService mongoDBService;

    @Autowired
    MongoConnectionDetails mongoConnectionDetails;

    @Test
    @DisplayName( "데이터베이스 생성 및 person collection에 index 생성 및 확인 테스트" )
    void create_mongo_database_test() throws Exception {
        String dbName = "createdb";
        boolean notExistDatabase = mongoDBService.isNotExistDatabase( dbName );
        Assertions.assertThat(notExistDatabase).isTrue();
        //database를 생성과 동시에 person collection에 'personName', 'GenderNationIdx' index를 생성한다.
        mongoDBService.createDatabase( dbName );
        notExistDatabase = mongoDBService.isNotExistDatabase(dbName);
        Assertions.assertThat(notExistDatabase).isFalse();

        final String indexName1 = "personName";
        final String indexName2 = "GenderNationIdx";
        Assertions.assertThat(
                mongoDBService.checkHasIndexPerson( dbName, indexName1 ) )
                .isTrue();

        Assertions.assertThat(
                mongoDBService.checkHasIndexPerson( dbName, indexName2 ) )
                .isTrue();
    }

    @Test
    @DisplayName( "PersonDocument를 MongoTemplate을 이용하여 저장하는 테스트" )
    void save_person_document_by_mongotemplate_test() {
        PersonDocument personDocument = PersonDocument.builder()
                .personId( UUID.randomUUID() )
                .personName( "test-name" )
                .personAge( 100 )
                .personGender( Gender.NONE )
                .comment( "test-name 코멘트" )
                .build();
        personDocument = mongoDBService.savePersonByMongoTemplate( personDocument );
        Optional<PersonDocument> optionalPersonDocument = mongoDBService.findPerson( personDocument.getPersonId() );
        Assertions.assertThat( optionalPersonDocument ).isPresent();
        PersonDocument savedPersonDocument = optionalPersonDocument.get();
        Assertions.assertThat( savedPersonDocument.getPersonId() ).isEqualTo( personDocument.getPersonId() );
    }

    @Test
    @DisplayName( "PersonDocument를 Repository를 이용하여 저장하는 테스트" )
    void save_person_document_by_repository_test() {
        PersonDocument personDocument = PersonDocument.builder()
                .personId( UUID.randomUUID() )
                .personName( "test-name" )
                .personAge( 100 )
                .personGender( Gender.NONE )
                .comment( "test-name 코멘트" )
                .build();
        personDocument = mongoDBService.savePersonByRepository( personDocument );
        Optional<PersonDocument> optionalPersonDocument = mongoDBService.findPerson( personDocument.getPersonId() );
        Assertions.assertThat( optionalPersonDocument ).isPresent();
        PersonDocument savedPersonDocument = optionalPersonDocument.get();
        Assertions.assertThat( savedPersonDocument.getPersonId() ).isEqualTo( personDocument.getPersonId() );
    }

    @Test
    @DisplayName( "동적으로 데이터베이스를 생성하여 ScopedValue를 이용하여 해당 데이터베이스에 데이터를 저장하는 테스트" )
    void create_database_dynamically_and_save_person_document_use_scoped_value_test() {
        String dynamicallyAddedDBName = "test2";
        boolean notExistDatabase = mongoDBService.isNotExistDatabase( dynamicallyAddedDBName );
        Assertions.assertThat(notExistDatabase).isTrue();
        mongoDBService.createDatabase( dynamicallyAddedDBName );
        notExistDatabase = mongoDBService.isNotExistDatabase( dynamicallyAddedDBName );
        Assertions.assertThat(notExistDatabase).isFalse();

        //dynamicallyAddedDBName 이 scoped value에 바인딩되어 있으면 "test2" 데이터베이스에 연결하여 저장한다.
        UUID newPersonId = UUID.randomUUID();
        ScopedValue.Carrier carrierForDBName = ScopedValues.getCarrierForDBName( dynamicallyAddedDBName );
        carrierForDBName.run( () -> {
            PersonDocument personDocument = PersonDocument.builder()
                    .personId( newPersonId )
                    .personName( "test-name-other-database" )
                    .personAge( 50 )
                    .personGender( Gender.FEMALE )
                    .comment( "다른 데이터베이스의 person 코멘트" )
                    .build();

            personDocument = mongoDBService.savePersonByMongoTemplate( personDocument );
            Optional<PersonDocument> optionalPersonDocument = mongoDBService.findPerson( personDocument.getPersonId() );
            Assertions.assertThat( optionalPersonDocument ).isPresent();
            PersonDocument savedPersonDocument = optionalPersonDocument.get();
            Assertions.assertThat( savedPersonDocument.getPersonId() ).isEqualTo( newPersonId );
            Assertions.assertThat( savedPersonDocument.getPersonId() ).isEqualTo( personDocument.getPersonId() );
        } );

        //dynamicallyAddedDBName 이 scoped value에 바인딩 되어 있지 않으면 기본 데이터베이스인 "test"에 연결하여 처리된다.
        Optional<PersonDocument> optionalPersonDocument = mongoDBService.findPerson( newPersonId );
        //test2 디비에 저장하였으므로 test 디비에서는 조회가 되지 않는다.
        Assertions.assertThat( optionalPersonDocument ).isEmpty();
    }

    @Test
    @DisplayName( "repository에 pageable 사용 테스트" )
    void use_repository_pageable_test() {
        //50:50 비율로 MALE, FEMALE 성별을 가진 100개의 PersonDocument 저장
        mongoDBService.savePersonByRepository100();
        long totalCount = 0;
        int prevPageNumber;
        int currentPageNumber;
        final int expectedTotalCount = 100;
        final int pageSize = 10;

        Page<PersonDocument> personDocuments;
        // page size 10, personAge 필드에 대해서 오름차순 정렬하여 paging
        Pageable pageable =
                PageRequest.of( 0, pageSize, Sort.by( Sort.Direction.ASC, "personAge") );

        do {
            personDocuments = mongoDBService.listByPageable( pageable );
            if ( !personDocuments.isEmpty() ) {
                totalCount += personDocuments.getTotalPages();
                prevPageNumber = pageable.getPageNumber();
                pageable = pageable.next();
                currentPageNumber = pageable.getPageNumber();
                Assertions.assertThat( currentPageNumber ).isEqualTo( prevPageNumber + 1 );
            }
        } while ( !personDocuments.isEmpty() );

        Assertions.assertThat( totalCount ).isEqualTo( expectedTotalCount );
    }

    @Test
    @DisplayName( "scoped value를 이용한 동적으로 다른 DB로 연결하여 작업 수행하는 테스트" )
    void save_other_database_dynamically_test() {
        final String dynamicallyAddedDBName = "test2";
        boolean notExistDatabase = mongoDBService.isNotExistDatabase( dynamicallyAddedDBName );
        Assertions.assertThat(notExistDatabase).isTrue();
        mongoDBService.createDatabase( dynamicallyAddedDBName );
        notExistDatabase = mongoDBService.isNotExistDatabase(dynamicallyAddedDBName);
        Assertions.assertThat(notExistDatabase).isFalse();

        final UUID personId = UUID.randomUUID();
        //test2 DB에 PersonDocument를 저장
        ScopedValue.Carrier carrierForDBName = ScopedValues.getCarrierForDBName( dynamicallyAddedDBName );
        carrierForDBName.run( () -> {
            PersonDocument personDocument = PersonDocument.builder()
                    .personId( personId )
                    .personName( "test-name-other-database" )
                    .personAge( 50 )
                    .personGender( Gender.FEMALE )
                    .comment( "다른 데이터베이스의 person 코멘트" )
                    .build();

            PersonDocument savedDocument = mongoDBService.savePersonByMongoTemplate( personDocument );
            Assertions.assertThat( savedDocument.getPersonId() ).isEqualTo( personId );
        } );

        // ScopedValue binding이 해제된 상태에서는 기본 DB (test) 에 연결
        // test2 DB에 저장했기 때문에 test DB에서는 조회되지 않는다.
        Assertions.assertThat( mongoDBService.findPerson( personId ) ).isEmpty();

        // ScopedValue가 binding 된 상태에서는 test2 DB에 연결하여 데이터를 조회한다.
        carrierForDBName.run( () ->
                Assertions.assertThat( mongoDBService.findPerson( personId ) ).isPresent() );
    }

    @Test
    @DisplayName( "embedded array field 테스트" )
    void embedded_array_field_test() {
        final String receiver = "receiver@test.com";
        MailSenderDocument.MailSenderDocumentBuilder builder = MailSenderDocument.builder();
        builder.id( UUID.randomUUID() ).address( receiver );

        final int arraySize = 100;
        //array field 에 100개의 Sender element 생성
        List<MailSenderDocument.Sender> senderList = new ArrayList<>();
        Stream.iterate(0, seed -> seed + 1).limit( arraySize )
                .forEach( index -> {
                    MailSenderDocument.Sender sender = MailSenderDocument.Sender.builder()
                            .address( STR."sender\{index}@test.com" )
                            .lastSentTime( Instant.now().minus( index, ChronoUnit.DAYS ) )
                            .build();
                    senderList.add( sender );
                } );

        MailSenderDocument mailSenderDocument = builder.senders( senderList ).build();
        mongoDBService.insertMailSender( mailSenderDocument );

        Optional<MailSenderDocument> optionalMailSenderDocument = mongoDBService.findByAddress( receiver );
        Assertions.assertThat( optionalMailSenderDocument ).isNotEmpty();

        MailSenderDocument findDocument = optionalMailSenderDocument.get();
        Assertions.assertThat( findDocument.getSenders().size() ).isEqualTo( arraySize );

        final String searchSender = "sender1@test.com";
        // find array field data by aggregation
        Optional<MailSenderDocument.CheckSenderResult> optionalCheckSenderResult =
                mongoDBService.checkExistMailSender( receiver, searchSender );
        Assertions.assertThat( optionalCheckSenderResult ).isNotEmpty();
        MailSenderDocument.CheckSenderResult checkSenderResult = optionalCheckSenderResult.get();
        Assertions.assertThat( checkSenderResult.getSenderAddress() ).isEqualTo( searchSender );

        Instant lastSentTime = checkSenderResult.getLastSentTime();
        Instant updateSentTime = lastSentTime.minus( 1, ChronoUnit.DAYS );
        // update lastSentTime field in array element
        mongoDBService.updateMailSender(
                receiver,
                MailSenderDocument.Sender.builder()
                        .address( searchSender )
                        .lastSentTime( updateSentTime )
                        .build());

        Instant updatedLastSentTime = mongoDBService.checkExistMailSender( receiver, searchSender )
                .map( MailSenderDocument.CheckSenderResult::getLastSentTime )
                .orElse( null );
        Assertions.assertThat( updatedLastSentTime ).isEqualTo( updateSentTime );
    }
}