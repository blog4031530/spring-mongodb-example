package com.example.springmongodb.events;

import com.example.springmongodb.data.Gender;
import com.example.springmongodb.documents.PersonDocument;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertCallback;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveCallback;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@Slf4j
public class MongoDBEntityCallbacks implements
        BeforeConvertCallback<PersonDocument>,
        BeforeSaveCallback<PersonDocument> {
    @Override
    public PersonDocument onBeforeConvert( PersonDocument entity, String collection ) {
        log.info( "before convert callback, entity: {}, collection: {}", entity, collection );
        if ( entity.getPersonGender() == null ) {
            entity.setPersonGender( Gender.MALE );
        }

        if ( !StringUtils.hasText( entity.getPersonNation() ) ) {
            entity.setPersonNation( "Korea" );
        }

        return entity;
    }

    @Override
    public PersonDocument onBeforeSave( PersonDocument entity, Document document, String collection ) {
        log.info( "before save callback, entity: {}, document: {}, collection: {}", entity, document, collection );
        return entity;
    }
}
