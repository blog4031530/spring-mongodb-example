package com.example.springmongodb.events;

import com.example.springmongodb.documents.PersonDocument;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.springframework.data.mongodb.core.mapping.event.*;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MongoDBLifeCycleEvents extends AbstractMongoEventListener<PersonDocument> {
    @Override
    public void onBeforeConvert( BeforeConvertEvent<PersonDocument> event ) {
        PersonDocument source = event.getSource();
        String collection = event.getCollectionName();
        log.info( "before convert person object: {}, collection: {}", source, collection );
    }

    @Override
    public void onBeforeSave( BeforeSaveEvent<PersonDocument> event ) {
        PersonDocument source = event.getSource();
        String collection = event.getCollectionName();
        log.info( "before save person object: {}, collection: {}", source, collection );
    }

    @Override
    public void onAfterSave( AfterSaveEvent<PersonDocument> event ) {
        PersonDocument source = event.getSource();
        String collection = event.getCollectionName();
        log.info( "after save person object: {}, collection: {}", source, collection );
    }

    @Override
    public void onAfterLoad( AfterLoadEvent<PersonDocument> event ) {
        Document source = event.getSource();
        String collection = event.getCollectionName();
        log.info( "after load document: {}, collection: {}", source, collection );
    }

    @Override
    public void onAfterConvert( AfterConvertEvent<PersonDocument> event ) {
        PersonDocument source = event.getSource();
        String collection = event.getCollectionName();
        log.info( "after convert person object: {}, collection: {}", source, collection );
    }

    @Override
    public void onAfterDelete( AfterDeleteEvent<PersonDocument> event ) {
        Document source = event.getSource();
        String collection = event.getCollectionName();
        log.info( "after delete document: {}, collection: {}", source, collection );
    }

    @Override
    public void onBeforeDelete( BeforeDeleteEvent<PersonDocument> event ) {
        Document source = event.getSource();
        String collection = event.getCollectionName();
        log.info( "before delete document: {}, collection: {}", source, collection );
    }
}
