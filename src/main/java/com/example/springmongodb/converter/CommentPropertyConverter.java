package com.example.springmongodb.converter;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.data.mongodb.core.convert.MongoConversionContext;
import org.springframework.data.mongodb.core.convert.MongoValueConverter;

public class CommentPropertyConverter implements MongoValueConverter<String, String> {

    @Override
    public String read( String value, MongoConversionContext context ) {
        return new String(Base64.decodeBase64( value ));
    }

    @Override
    public String write( String value, MongoConversionContext context ) {
        return new String(Base64.encodeBase64( value.getBytes(), false ));
    }
}
