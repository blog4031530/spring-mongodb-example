package com.example.springmongodb.converter;

import com.example.springmongodb.data.Gender;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.util.Optional;

/**
 * TypeConverter 는 org.springframework.core.convert.converter.Converter 인스턴스를 구현한다.
 * mongodb 에서 데이터를 읽을 때 String 타입의 데이터를 Gender 타입의 데이터로 변환한다.
 */
@ReadingConverter
public class ReadGenderTypeConverter implements Converter<String, Gender> {

    @Override
    public Gender convert( String gender ) {
        Optional<Gender> optionalGender = Gender.genderByKr( gender );
        return optionalGender.orElse( Gender.NONE );
    }
}
