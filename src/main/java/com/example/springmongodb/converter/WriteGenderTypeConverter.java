package com.example.springmongodb.converter;

import com.example.springmongodb.data.Gender;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

/**
 * TypeConverter 는 org.springframework.core.convert.converter.Converter 인스턴스를 구현한다.
 * mongodb 에서 데이터를 쓸 때 Gender 타입의 데이터를 String 타입의 데이터로 변환한다.
 */
@WritingConverter
public class WriteGenderTypeConverter implements Converter<Gender, String> {
    @Override
    public String convert( Gender source ) {
        return source.getKr();
    }
}
