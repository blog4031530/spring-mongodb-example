package com.example.springmongodb.data;

public class ScopedValues {
    private static final ScopedValue<String> DATABASE_NAME = ScopedValue.newInstance();

    public static ScopedValue.Carrier getCarrierForDBName(String dbName) {
        return ScopedValue.where( DATABASE_NAME, dbName );
    }

    public static ScopedValue<String> getScopedValueForDBName() {
        return DATABASE_NAME;
    }
}
