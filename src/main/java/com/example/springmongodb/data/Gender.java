package com.example.springmongodb.data;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum Gender {
    NONE("성별 없음"),
    MALE("남성"),
    FEMALE("여성"),
    ;

    final String kr;
    Gender( String kr ) {
        this.kr = kr;
    }

    public static Optional<Gender> genderByKr( String kr) {
        return Arrays.stream( Gender.values() )
                .filter( gender -> gender.getKr().equals( kr ) )
                .findFirst();
    }
}
