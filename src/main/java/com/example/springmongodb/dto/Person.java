package com.example.springmongodb.dto;

import com.example.springmongodb.data.Gender;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

/**
 * Indexed와 CompoundIndexes, CompoundIndex 어노테이션은 auto-index-creation: true인 경우 적용된다.
 */

@Getter
@ToString
public class Person {
    private final UUID personId;
    private final String personName;
    private final Integer personAge;
    @Setter private Gender personGender;
    @Setter private String personNation;


    @Builder
    public Person( UUID personId, String personName, Integer personAge, Gender personGender, String personNation) {
        this.personId = personId;
        this.personName = personName;
        this.personAge = personAge;
        this.personGender = personGender;
        this.personNation = personNation;
    }
}
