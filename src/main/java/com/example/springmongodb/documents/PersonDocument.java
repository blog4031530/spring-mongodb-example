package com.example.springmongodb.documents;

import com.example.springmongodb.data.Gender;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

/**
 * Indexed와 CompoundIndexes, CompoundIndex 어노테이션은 auto-index-creation: true인 경우 적용된다.
 */
@Document(collection = "person")
@Getter
@ToString
/**
 * 여기서 지정된 index는 기본 database에 대해서만 생성된다.
 * 동적으로 추가된 database에 대해서는 별도로 인덱스를 생성해줘야 한다.
 */
@CompoundIndexes(value = {
        @CompoundIndex(name = "GenderNationIdx", def = "{'personGender': 1, 'personNation': -1}" )
})
public class PersonDocument extends Auditable {
    @Id
    private final UUID personId;
    //spring.data.mongodb.auto-index-creation 설정이 true인 경우 @Indexed로 지정된 컬럼에 index가 생성된다.
    @Indexed
    private final String personName;
    private final Integer personAge;
    @Setter private Gender personGender;
    @Setter private String personNation;
    private final String comment;


    @Builder
    public PersonDocument( UUID personId,
                           String personName,
                           Integer personAge,
                           Gender personGender,
                           String personNation,
                           String comment) {
        this.personId = personId;
        this.personName = personName;
        this.personAge = personAge;
        this.personGender = personGender;
        this.personNation = personNation;
        this.comment = comment;
    }

    @Override
    public UUID getId() {
        return personId;
    }
}
