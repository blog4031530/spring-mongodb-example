package com.example.springmongodb.documents;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@ToString( callSuper = true )
@Document( collection = "mail_senders" )
public class MailSenderDocument {

    @Id
    protected UUID id;
    @Indexed(unique = true) private String address;
    private List<Sender> senders;

    @SuppressWarnings( "java:S107" )
    @Builder
    public MailSenderDocument( UUID id, String address, List<Sender> senders ) {
        this.id = id;
        this.address = address;
        this.senders = (senders != null) ? senders : new ArrayList<>();
    }


    @Getter
    @Setter
    @NoArgsConstructor
    public static class Sender {
        private String address;
        private Instant lastSentTime;

        @Builder
        public Sender( String address, Instant lastSentTime ) {
            this.address = address;
            this.lastSentTime = lastSentTime;
        }

        @Override
        public boolean equals( Object o ) {
            if ( this == o ) return true;
            if ( o == null || getClass() != o.getClass() ) return false;
            Sender sender = ( Sender )o;
            return Objects.equals( address, sender.address );
        }

        @Override
        public int hashCode() {
            return Objects.hash( address );
        }
    }

    @ToString
    @NoArgsConstructor
    @Getter
    public static class CheckSenderResult {
        private UUID id;
        private String senderAddress;
        private Instant lastSentTime;
    }
}
