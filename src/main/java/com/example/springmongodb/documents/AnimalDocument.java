package com.example.springmongodb.documents;

import com.example.springmongodb.converter.CommentPropertyConverter;
import com.example.springmongodb.data.Gender;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.convert.ValueConverter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;
import java.util.UUID;

@Document(collection = "animal")
@Getter
@ToString
public class AnimalDocument {
    @Id
    private final UUID animalId;
    //spring.data.mongodb.auto-index-creation 설정이 true인 경우 @Indexed로 지정된 컬럼에 index가 생성된다.
    @Indexed
    private final String animalName;
    private final Integer animalAge;
    @Setter private Gender animalGender;
    @Setter private String animalNation;
    private final Map<String, String> mapData;
    @ValueConverter( CommentPropertyConverter.class)
    private final String comment;


    @Builder
    public AnimalDocument( UUID animalId,
                           String animalName,
                           Integer animalAge,
                           Gender animalGender,
                           String animalNation,
                           Map<String, String> mapData,
                           String comment) {
        this.animalId = animalId;
        this.animalName = animalName;
        this.animalAge = animalAge;
        this.animalGender = animalGender;
        this.animalNation = animalNation;
        this.mapData = mapData;
        this.comment = comment;
    }
}
