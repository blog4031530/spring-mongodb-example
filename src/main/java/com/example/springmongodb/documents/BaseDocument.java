package com.example.springmongodb.documents;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.*;
import org.springframework.data.domain.Persistable;

import java.time.Instant;
import java.util.UUID;


@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
@Getter
@Setter
public abstract class BaseDocument implements Persistable<UUID> {
    @Id
    protected UUID id;
    @CreatedDate
    protected Instant createdTime;
    @CreatedBy
    protected String createdBy;
    @LastModifiedDate
    protected Instant lastModifiedTime;
    @LastModifiedBy
    protected String lastModifiedBy;

    @JsonIgnore
    @Override
    public boolean isNew() {
        return (createdTime == null);
    }
}
