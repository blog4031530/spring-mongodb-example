package com.example.springmongodb;

import com.example.configuration.MongoDBConfigurationByDirect;
import com.example.springmongodb.service.MongoDBService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
//MongoDBConfigurationByDirect.class를 사용하고자 하면 MongoDBConfigurationBySupport.class를 주석 처리한다.
//MongoDBConfigurationBySupport.class를 사용하고자 하면 MongoDBConfigurationByDirect.class를 주석 처리한다.
//AutoConfiguration에 의해서 연결 구성을 하려면 둘 다 주석 처리한다.
@Import( value = {
//        MongoDBConfigurationBySupport.class,
        MongoDBConfigurationByDirect.class
})
public class SpringMongodbApplication implements CommandLineRunner {

    private final MongoDBService mongoDBService;

    public static void main( String[] args ) {
        SpringApplication.run( SpringMongodbApplication.class, args );
    }

    @Override
    public void run( String... args ) {
        //create sample data in test DB
        //repository pageable 확인용 샘플 데이터
        mongoDBService.savePersonByRepository100();
    }
}
