package com.example.springmongodb.repository.mailsender;

import com.example.springmongodb.documents.MailSenderDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MailSenderRepository extends MongoRepository<MailSenderDocument, UUID>,
        CustomMailSenderRepository, CustomizedSave<MailSenderDocument> {

    Optional<MailSenderDocument> findByAddress(String address);
}
