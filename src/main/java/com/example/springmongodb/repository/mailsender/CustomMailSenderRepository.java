package com.example.springmongodb.repository.mailsender;

import com.example.springmongodb.documents.MailSenderDocument;

import java.util.Optional;

public interface CustomMailSenderRepository {
    MailSenderDocument insertMailSender( MailSenderDocument mailSenderDocument );
    Optional<MailSenderDocument.CheckSenderResult> checkExistMailSender( String address, String senderAddress);
    void updateMailSender( String address, MailSenderDocument.Sender sender );
    void removeSenderExpiredDate(Integer threshold);
}
