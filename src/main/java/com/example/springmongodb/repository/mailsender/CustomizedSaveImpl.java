package com.example.springmongodb.repository.mailsender;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;

@Slf4j
@RequiredArgsConstructor
public class CustomizedSaveImpl<T> implements CustomizedSave<T> {
    private final MongoTemplate mongoTemplate;

    @Override
    public <S extends T> S save( S entity ) {
        log.info( "called customized save" );
        return mongoTemplate.save( entity );
    }
}
