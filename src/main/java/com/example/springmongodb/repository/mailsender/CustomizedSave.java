package com.example.springmongodb.repository.mailsender;

public interface CustomizedSave<T>{
    <S extends T> S save(S entity);
}
