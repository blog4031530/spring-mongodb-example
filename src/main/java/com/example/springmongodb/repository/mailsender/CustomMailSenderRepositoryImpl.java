package com.example.springmongodb.repository.mailsender;

import com.example.springmongodb.documents.MailSenderDocument;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.query.Criteria.where;

@RequiredArgsConstructor
public class CustomMailSenderRepositoryImpl implements CustomMailSenderRepository{
    private final static String COLLECTION_NAME_MAIL_SENDER = "mail_senders";

    private final MongoTemplate mongoTemplate;


    @Override
    public MailSenderDocument insertMailSender( MailSenderDocument mailSenderDocument ) {
        return mongoTemplate.save( mailSenderDocument );
    }

    @Override
    public Optional<MailSenderDocument.CheckSenderResult> checkExistMailSender( String address, String senderAddress ) {

        AggregationOperation matchAddress = Aggregation.match( where( "address" ).is( address ) );
        ArrayOperators.ArrayOperatorFactory arrayOperatorFactory = ArrayOperators.arrayOf( "senders.address" );
        AggregationExpression inExpression = arrayOperatorFactory.containsValue( senderAddress );
        AggregationExpression indexOfArrayExpression = arrayOperatorFactory.indexOf( senderAddress );
        AggregationExpression arrayElemAtExpression = ArrayOperators.arrayOf( "senders" ).elementAt( indexOfArrayExpression );

        AggregationExpression conditionalExpression = ConditionalOperators.when( inExpression )
                .thenValueOf( arrayElemAtExpression )
                .otherwise( MailSenderDocument.Sender.builder().build() );

        AggregationOperation projection1 = project()
                .and( conditionalExpression ).as( "extractedSender" );

        AggregationOperation projection2 = project()
                .and( "extractedSender.address" ).as( "senderAddress" )
                .and( "extractedSender.lastSentTime" ).as("lastSentTime");

        Aggregation aggregation = Aggregation.newAggregation( matchAddress, projection1, projection2 );
        AggregationResults<MailSenderDocument.CheckSenderResult> aggregate = mongoTemplate.aggregate( aggregation, COLLECTION_NAME_MAIL_SENDER, MailSenderDocument.CheckSenderResult.class );
        MailSenderDocument.CheckSenderResult checkSenderResult = aggregate.getUniqueMappedResult();

        AggregationResults<Document> aggregate2 = mongoTemplate.aggregate( aggregation, COLLECTION_NAME_MAIL_SENDER, Document.class );
        Document uniqueMappedResult = aggregate2.getUniqueMappedResult();

        return Optional.ofNullable( checkSenderResult );
    }

    @Override
    public void updateMailSender( String address, MailSenderDocument.Sender sender ) {
        Optional<MailSenderDocument.CheckSenderResult> optionalCheckSenderResult = checkExistMailSender( address, sender.getAddress() );
        optionalCheckSenderResult.ifPresentOrElse( checkSenderResult -> {
            // update
            Query query = new Query();
            Update update = new Update();
            if ( StringUtils.hasText( checkSenderResult.getSenderAddress() ) ) {
                query.addCriteria( where("_id").is( checkSenderResult.getId() )
                        .and( "senders.address" ).is( sender.getAddress() ) );
                update.set( "senders.$.lastSentTime", sender.getLastSentTime() );
            }
            // addToSet | push
            else {
                query.addCriteria( where( "_id").is( checkSenderResult.getId()) );
                update.addToSet( "senders", sender );
            }

            mongoTemplate.updateFirst( query, update, COLLECTION_NAME_MAIL_SENDER );
        }, () -> {
            MailSenderDocument mailSenderDocument = MailSenderDocument.builder()
                    .id( UUID.randomUUID() )
                    .address( address )
                    .senders( List.of( sender ) )
                    .build();
            insertMailSender( mailSenderDocument );
        } );
    }

    @Override
    public void removeSenderExpiredDate( Integer threshold ) {
        Instant thresholdDaysAgo = Instant.now().minus( threshold, ChronoUnit.DAYS );
        Query query = new Query().addCriteria( Criteria.where( "senders.lastSentTime" ).lt( thresholdDaysAgo ) );
        Update update = new Update().pull( "senders", Query.query(Criteria.where( "lastSentTime" ).lt( thresholdDaysAgo )) );
        mongoTemplate.updateMulti( query, update, COLLECTION_NAME_MAIL_SENDER );
    }
}
