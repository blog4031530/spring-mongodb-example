package com.example.springmongodb.repository;

import com.example.springmongodb.data.Gender;
import com.example.springmongodb.documents.PersonDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PersonRepository extends MongoRepository<PersonDocument, UUID> {

    /*
    성별에 따른 Document 리스트를 추출 한다.
     */
    List<PersonDocument> findByPersonGender( Gender gender );

    /*
    personAge 필드의 값이 파라미터 age 보다 큰 Document 리스트를 추출 한다.
     */
    @Query("{" +
            "'personAge': { $gt: ?0 }" +
            "}")
    List<PersonDocument> findByPersonAgeGreaterThan(Integer age);

    /*
    성별에 따른 Document 페이지 목록을 추출 한다.
     */
    Page<PersonDocument> findByPersonGender( Gender gender, Pageable pageable );
}
