package com.example.springmongodb.audit;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {

        //spring security를 사용한다면 SecurityContextHolder로부터 현재 인증된 아이디 정보를 설정할 수 있다.
        /*
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || !authentication.isAuthenticated()) {
            return Optional.of("system");
        }

        return Optional.ofNullable(authentication.getName());
         */
        return Optional.of("system");
    }
}
