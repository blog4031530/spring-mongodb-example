package com.example.springmongodb.controller;

import com.example.springmongodb.data.Gender;
import com.example.springmongodb.documents.PersonDocument;
import com.example.springmongodb.service.MongoDBService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mongodb/persons")
@RequiredArgsConstructor
public class PersonController {
    private final MongoDBService mongoDBService;

    @GetMapping(value = "/gender", produces = MediaType.APPLICATION_JSON_VALUE )
    public Page<PersonDocument> listPersonByGender( @RequestParam Gender gender,
                                                    Pageable pageable ) {
        return mongoDBService.listByGenderPageable(gender, pageable);
    }
}
