package com.example.springmongodb.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.CompoundIndexDefinition;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.index.IndexOperations;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class MongoDBIndexService {

    private final MongoTemplate mongoTemplate;
    private final static String COLLECTION_NAME = "person";


    /**
     * 애플리케이션 기동시점에 이미 존재하는 모든 database에 index를 새롭게 생성하거나 조정해야할 필요가 있을 때
     * 아래 호출이 필요할 수 있다. 여기 샘플 코드에서는 불필요하다.
     */
    /*
    private final MongoClient mongoClient;
    private final List<String> skipList = List.of("admin", "config", "local");

    @PostConstruct
    void initIndex() {
        MongoIterable<String> databaseNames = mongoClient.listDatabaseNames();
        StreamUtils.createStreamFromIterator( databaseNames.iterator() )
                        .filter( dbName -> !skipList.contains( dbName ) )
                        .forEach( dbName -> {
                            ScopedValue.Carrier carrierForDBName = ScopedValues.getCarrierForDBName( dbName );
                            carrierForDBName.run( this::createIndexPerson );
                        } );
    }*/

    /**
     * person collection에 index를 생성하는 메서드
     */
    public void createIndexPerson() {
        IndexOperations indexOperations = mongoTemplate.indexOps( COLLECTION_NAME );
        //personName 오름차순 인덱스 생성
        //auto-index-creation이 true 인 경우 @Indexed가 붙은 필드명 이름으로 인덱스가 자동 생성된다.
        //conflict 오류를 피하려면 named를 지정하여 자동으로 생성되었던 필드명으로 인덱스 이름을 지정해야 한다.
        //혹은 auto-index-creation을 false로 지정하고 직접 인덱스를 생성한다.
        indexOperations.ensureIndex(
                new Index().on( "personName", Sort.Direction.ASC )
                        .named( "personName" ));

        indexOperations.ensureIndex( new CompoundIndexDefinition(
                Document.parse( "{personGender: 1, personNation: -1}" )
            ).named( "GenderNationIdx" )
        );
    }

    /**
     * person collection에 지정된 index 이름의 index가 존재하는지 체크하는 메서드
     * @param indexName 체크하고자 하는 인덱스 이름
     * @return true: 존재함, false: 존재하지 않음
     */
    public Boolean checkHasIndexPerson(String indexName) {
        return mongoTemplate.execute( COLLECTION_NAME, collection ->
                Streamable.of( collection.listIndexes( Document.class ) )
                        .stream()
                        .map( document -> document.get( "name" ) )
                        .anyMatch( indexName::equals )
        );
    }
}
