package com.example.springmongodb.service;

import com.example.springmongodb.data.Gender;
import com.example.springmongodb.data.ScopedValues;
import com.example.springmongodb.documents.MailSenderDocument;
import com.example.springmongodb.documents.PersonDocument;
import com.example.springmongodb.repository.PersonRepository;
import com.example.springmongodb.repository.mailsender.MailSenderRepository;
import com.mongodb.client.MongoClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.util.StreamUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@Slf4j
public class MongoDBService {
    private final MongoClient mongoClient;
    private final MongoTemplate mongoTemplate;
    private final PersonRepository personRepository;
    private final MailSenderRepository mailSenderRepository;
    private final MongoDBIndexService mongoDBIndexService;

    public PersonDocument savePersonByMongoTemplate( PersonDocument personDocument ) {
        return mongoTemplate.insert( personDocument );
    }

    public Optional<PersonDocument> findPerson(UUID id) {
        return personRepository.findById( id );
    }

    public PersonDocument savePersonByRepository( PersonDocument personDocument ) {
        return personRepository.save( personDocument );
    }

    public void savePersonByRepository100() {
        Stream.iterate( 0, seed -> seed + 1 ).limit( 100 )
                .forEach( index -> {
                    PersonDocument personDocument = PersonDocument.builder()
                            .personId( UUID.randomUUID() )
                            .personName( STR."test-name\{index}" )
                            .personAge( index )
                            .personGender( index % 2 == 0 ? Gender.MALE : Gender.FEMALE )
                            .comment( STR."test-name 코멘트\{index}"  )
                            .build();
                    personRepository.save( personDocument );
                } );
    }

    public List<PersonDocument> findByGender(Gender gender) {
        return personRepository.findByPersonGender( gender );
    }

    public List<PersonDocument> findByAgeGreaterThan(Integer age) {
        return personRepository.findByPersonAgeGreaterThan( age );
    }

    public Page<PersonDocument> listByGenderPageable( Gender gender, Pageable pageable ) {
        return personRepository.findByPersonGender( gender, pageable );
    }

    public Page<PersonDocument> listByPageable( Pageable pageable ) {
        return personRepository.findAll(pageable);
    }

    public void insertMailSender( MailSenderDocument mailSenderDocument ) {
        mailSenderRepository.save( mailSenderDocument );
    }

    public Optional<MailSenderDocument.CheckSenderResult> checkExistMailSender( String address, String senderAddress) {
        return mailSenderRepository.checkExistMailSender( address, senderAddress );
    }

    public Optional<MailSenderDocument> findByAddress(String receiverAddress) {
        return mailSenderRepository.findByAddress( receiverAddress );
    }

    public void updateMailSender( String address, MailSenderDocument.Sender sender ) {
        mailSenderRepository.updateMailSender( address, sender );
    }

    public void removeSenderExpiredDate(Integer threshold) {
        mailSenderRepository.removeSenderExpiredDate( threshold );
    }

    public boolean isNotExistDatabase(String databaseName) {
        return StreamUtils.createStreamFromIterator( mongoClient.listDatabaseNames().iterator() )
                .noneMatch( dbName -> dbName.equals( databaseName ) );
    }

    public void createDatabase(String databaseName) {
        ScopedValue.Carrier carrierForDBName = ScopedValues.getCarrierForDBName( databaseName );
        carrierForDBName.run( mongoDBIndexService::createIndexPerson );
    }

    public Boolean checkHasIndexPerson(String databaseName, String indexName) throws Exception {
        ScopedValue.Carrier carrierForDBName = ScopedValues.getCarrierForDBName( databaseName );
        return carrierForDBName.call( () -> mongoDBIndexService.checkHasIndexPerson( indexName ) );
    }
}
