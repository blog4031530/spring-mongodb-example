package com.example.configuration;

import com.example.springmongodb.audit.AuditorAwareImpl;
import com.example.springmongodb.converter.CommentPropertyConverter;
import com.example.springmongodb.converter.ReadGenderTypeConverter;
import com.example.springmongodb.converter.WriteGenderTypeConverter;
import com.example.springmongodb.data.ScopedValues;
import com.example.springmongodb.documents.PersonDocument;
import com.mongodb.MongoClientSettings;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.mongo.*;
import org.springframework.boot.ssl.SslBundles;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.WriteResultChecking;
import org.springframework.data.mongodb.core.convert.*;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.concurrent.TimeUnit;

@Configuration
@RequiredArgsConstructor
@Slf4j
@EnableMongoAuditing(auditorAwareRef = "auditorAwareImpl")
@EnableMongoRepositories(basePackages = "com.example.springmongodb.repository")
public class MongoDBConfigurationByDirect {

    @Bean
    public AuditorAware<String> auditorAwareImpl() {
        return new AuditorAwareImpl();
    }

    @Bean
    public MongoClientSettings mongoClientSettings() {
        return MongoClientSettings.builder()
                .applyToConnectionPoolSettings( connectionPoolSettingsBuilder ->
                        connectionPoolSettingsBuilder
                                .maxSize( 50 )
                                .minSize( 10 )
                                .maxConnectionLifeTime( 1, TimeUnit.MINUTES )
                )
                .applyToSocketSettings( socketSetting ->
                        socketSetting
                                .connectTimeout( 30, TimeUnit.SECONDS )
                                .readTimeout( 30, TimeUnit.SECONDS )
                )
                .build();
    }

    @Bean
    public MappingMongoConverter mongoConverter( MongoDatabaseFactory mongoDbFactory, MongoMappingContext mongoMappingContext ) {
        DbRefResolver dbRefResolver = new DefaultDbRefResolver( mongoDbFactory );
        MappingMongoConverter mongoConverter = new MappingMongoConverter( dbRefResolver, mongoMappingContext );
        mongoConverter.setMapKeyDotReplacement( "#DOT#" );
        mongoConverter.setCustomConversions( customConversions() );
        return mongoConverter;
    }

    @Bean
    public StandardMongoClientSettingsBuilderCustomizer standardMongoSettingsCustomizer( MongoProperties properties,
                                                                                         MongoConnectionDetails connectionDetails,
                                                                                         ObjectProvider<SslBundles> sslBundles) {
        return new StandardMongoClientSettingsBuilderCustomizer(
                connectionDetails.getConnectionString(),
                properties.getUuidRepresentation(),
                properties.getSsl(),
                sslBundles.getIfAvailable());
    }

    //create MongoTemplate Bean example
    @Bean
    public MongoTemplate mongoTemplate( MongoDatabaseFactory factory, MongoConverter converter ) {
        MongoTemplate mongoTemplate = new MongoTemplate( factory, converter );
        mongoTemplate.setWriteResultChecking( WriteResultChecking.EXCEPTION );
        mongoTemplate.setReadPreference( ReadPreference.secondaryPreferred() );
        mongoTemplate.setWriteConcern( WriteConcern.W1.withJournal( true ).withWTimeout( 30, TimeUnit.SECONDS ) );
//        mongoTemplate.setWriteConcernResolver( action -> {
//            if ( action.getEntityType().getSimpleName().contains( "Person" ) ) {
//                return WriteConcern.W1;
//            } else if ( action.getMongoActionOperation() == MongoActionOperation.REPLACE ) {
//                return WriteConcern.JOURNALED;
//            } else if ( action.getCollectionName().equals( "user" ) ) {
//                return WriteConcern.MAJORITY;
//            } else {
//                return action.getDefaultWriteConcern();
//            }
//        } );

        /**
         * MongoDBEntityCallbacks 클래스 대신에 아래와 같이 callback 등록이 가능하다.
         */
        /*mongoTemplate.setEntityCallbacks( EntityCallbacks.create(
                ( BeforeConvertCallback<Person> )( entity, collection ) -> {
                    log.info( "before convert callback, entity: {}, collection: {}", entity, collection );
                    if ( entity.getPersonGender() == null ) {
                        entity.setPersonGender( Gender.MALE );
                    }

                    if ( !StringUtils.hasText( entity.getPersonNation() ) ) {
                        entity.setPersonNation( "Korea" );
                    }

                    return entity;
                },
                ( BeforeSaveCallback<Person> )( entity, document, collection) -> {
                    log.info( "before save callback, entity: {}, document: {}, collection: {}", entity, document, collection );
                    return entity;
                }
        ) );*/
        return mongoTemplate;
    }

    //MongoDatabaseFactory 빈을 직접 생성하는 경우 MongoClient 빈도 생성해 줘야 한다.
    @Bean
    public MongoClient mongo(ObjectProvider<MongoClientSettingsBuilderCustomizer> builderCustomizers,
                             MongoClientSettings settings) {
        return new MongoClientFactory(builderCustomizers.orderedStream().toList()).createMongoClient(settings);
    }

    @Bean
    public MongoDatabaseFactory mongoDatabaseFactory( MongoClient mongoClient,
                                                      MongoProperties properties,
                                                      MongoConnectionDetails connectionDetails) {
        String defaultDatabase = properties.getDatabase();
        if (defaultDatabase == null) {
            defaultDatabase = connectionDetails.getConnectionString().getDatabase();
        }

        //SimpleMongoClientDatabaseFactory를 상속하여 doGetMongoDatabase를 override
        //doGetMongoDatabase는 ScopedValue 타입의 DATABASE_NAME에 db명이 지정된 경우 해당 database를 리턴한다.
        //ScopedValue 타입의 DATABASE_NAME에 db명이 지정되지 않은 경우 기본 database를 리턴한다.
        return new SimpleMongoClientDatabaseFactory(mongoClient, defaultDatabase) {
            @Override
            protected MongoDatabase doGetMongoDatabase( String dbName ) {
                return super.doGetMongoDatabase( ScopedValues.getScopedValueForDBName().orElse( dbName ) );
            }
        };
    }

    @Bean
    public MongoCustomConversions customConversions() {
        return MongoCustomConversions.create(
                adapter -> {
                    //property converter 등록
                    adapter.configurePropertyConversions(
                            registrar -> registrar.registerConverter(
                                    PersonDocument.class,
                                    "comment",
                                    new CommentPropertyConverter() ) );
                    //type converter 등록
                    adapter.registerConverter( new ReadGenderTypeConverter() );
                    adapter.registerConverter( new WriteGenderTypeConverter() );
                }
        );
    }
}
