package com.example.configuration;

import com.example.springmongodb.audit.AuditorAwareImpl;
import com.example.springmongodb.converter.CommentPropertyConverter;
import com.example.springmongodb.converter.ReadGenderTypeConverter;
import com.example.springmongodb.converter.WriteGenderTypeConverter;
import com.example.springmongodb.data.ScopedValues;
import com.example.springmongodb.documents.PersonDocument;
import com.mongodb.MongoClientSettings;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.mongo.MongoConnectionDetails;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.ssl.SslBundle;
import org.springframework.boot.ssl.SslBundles;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoActionOperation;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.WriteResultChecking;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.util.Assert;

import java.util.concurrent.TimeUnit;

@Configuration
@RequiredArgsConstructor
@EnableMongoAuditing(auditorAwareRef = "auditorAwareImpl")
public class MongoDBConfigurationBySupport extends AbstractMongoClientConfiguration {
    private final MongoConnectionDetails mongoConnectionDetails;
    private final MongoProperties mongoProperties;
    private final ObjectProvider<SslBundles> sslBundlesObjectProvider;

    @Bean
    public AuditorAware<String> auditorAwareImpl() {
        return new AuditorAwareImpl();
    }

    @Bean
    @Override
    @NonNull
    public MongoClient mongoClient() {
        return super.mongoClient();
    }

    @Override
    @NonNull
    public String getDatabaseName() {
        //test는 기본 database name
        return mongoConnectionDetails.getConnectionString().getDatabase();
    }

    @Override
    public MongoDatabaseFactory mongoDbFactory() {
        return new SimpleMongoClientDatabaseFactory(mongoClient(), getDatabaseName()) {
            @Override
            protected MongoDatabase doGetMongoDatabase( String dbName ) {
                return super.doGetMongoDatabase( ScopedValues.getScopedValueForDBName().orElse( dbName ) );
            }
        };
    }

    @Override
    @NonNull
    public MappingMongoConverter mappingMongoConverter( MongoDatabaseFactory databaseFactory, MongoCustomConversions customConversions, MongoMappingContext mappingContext ) {
        MappingMongoConverter mappingMongoConverter = super.mappingMongoConverter( databaseFactory, customConversions, mappingContext );
        mappingMongoConverter.setMapKeyDotReplacement( "#DOT#" );
        return mappingMongoConverter;
    }

    @Override
    public MongoTemplate mongoTemplate( MongoDatabaseFactory databaseFactory, MappingMongoConverter converter ) {
        MongoTemplate mongoTemplate = super.mongoTemplate( databaseFactory, converter );
        mongoTemplate.setWriteResultChecking( WriteResultChecking.EXCEPTION );
        mongoTemplate.setReadPreference( ReadPreference.secondaryPreferred() );
        mongoTemplate.setWriteConcern( WriteConcern.W1.withJournal( true ).withWTimeout( 30, TimeUnit.SECONDS ) );
        mongoTemplate.setWriteConcernResolver( action -> {
            if ( action.getEntityType().getSimpleName().contains( "Person" ) ) {
                return WriteConcern.W1;
            } else if ( action.getMongoActionOperation() == MongoActionOperation.REPLACE ) {
                return WriteConcern.JOURNALED;
            } else if ( action.getCollectionName().equals( "user" ) ) {
                return WriteConcern.MAJORITY;
            } else {
                return action.getDefaultWriteConcern();
            }
        } );

        /**
         * MongoDBEntityCallbacks 클래스 대신에 아래와 같이 callback 등록이 가능하다.
         */
        /*mongoTemplate.setEntityCallbacks( EntityCallbacks.create(
                ( BeforeConvertCallback<Person> )( entity, collection ) -> {
                    log.info( "before convert callback, entity: {}, collection: {}", entity, collection );
                    if ( entity.getPersonGender() == null ) {
                        entity.setPersonGender( Gender.MALE );
                    }

                    if ( !StringUtils.hasText( entity.getPersonNation() ) ) {
                        entity.setPersonNation( "Korea" );
                    }

                    return entity;
                },
                ( BeforeSaveCallback<Person> )( entity, document, collection) -> {
                    log.info( "before save callback, entity: {}, document: {}, collection: {}", entity, document, collection );
                    return entity;
                }
        ) );*/

        return mongoTemplate;
    }

    @Override
    protected void configureClientSettings( MongoClientSettings.Builder builder ) {
        builder.applyConnectionString( mongoConnectionDetails.getConnectionString() )
                .uuidRepresentation( mongoProperties.getUuidRepresentation() )
                .applyToConnectionPoolSettings( connectionPoolSettingsBuilder ->
                        connectionPoolSettingsBuilder
                                .maxSize( 50 )
                                .minSize( 10 )
                                .maxConnectionLifeTime( 1, TimeUnit.MINUTES )
                )
                .applyToSocketSettings( socketSetting ->
                        socketSetting
                                .connectTimeout( 30, TimeUnit.SECONDS )
                                .readTimeout( 30, TimeUnit.SECONDS )
                );

        if ( mongoProperties.getSsl().isEnabled() ) {
            builder.applyToSslSettings( sslSetting -> {
                if ( mongoProperties.getSsl().getBundle() != null ) {
                    sslSetting.enabled( true );
                    SslBundles sslBundles = sslBundlesObjectProvider.getIfAvailable();
                    SslBundle sslBundle = sslBundles.getBundle( mongoProperties.getSsl().getBundle() );
                    Assert.state(!sslBundle.getOptions().isSpecified(), "SSL options cannot be specified with MongoDB");
                    sslSetting.context( sslBundle.createSslContext() );
                }
            } );
        }
    }

    @Override
    protected void configureConverters( MongoCustomConversions.MongoConverterConfigurationAdapter converterConfigurationAdapter ) {
        converterConfigurationAdapter.registerConverter( new ReadGenderTypeConverter() );
        converterConfigurationAdapter.registerConverter( new WriteGenderTypeConverter() );
        converterConfigurationAdapter.configurePropertyConversions(
                registrar -> registrar.registerConverter(
                        PersonDocument.class,
                        "comment",
                        new CommentPropertyConverter() ) );
    }
}
